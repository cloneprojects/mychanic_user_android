package com.app.mymechanic.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.mymechanic.R;
import com.app.mymechanic.activities.AppSettings;
import com.app.mymechanic.activities.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * Created by admin on 12-06-2018.
 */

public class TaxAdapter extends RecyclerView.Adapter<TaxAdapter.TaxViewHolder> {
    private Context context;
    private JSONArray addresses;
    private String TAG = TaxAdapter.class.getSimpleName();

    public TaxAdapter(Context context, JSONArray addresses) {
        this.context = context;
        this.addresses = addresses;
    }

    @Override
    public TaxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.tax_items, parent, false);

        return new TaxViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TaxViewHolder holder, int position) {
        JSONObject jsonObject = addresses.optJSONObject(holder.getAdapterPosition());
        Log.e("taxman", "jsonObject: " + jsonObject);
        holder.taxName.setText(jsonObject.optString("taxname"));
        holder.taxPercentage.setText(jsonObject.optString("tax_amount"));
        holder.bookingGst.setText(jsonObject.optString("tax_totalamount"));

    }


    @Override
    public int getItemCount() {
        if (addresses.length() != 0) {
            return addresses.length();
        } else {
            return 0;
        }
    }

    class TaxViewHolder extends RecyclerView.ViewHolder {
        TextView taxName, taxPercentage, bookingGst;


        TaxViewHolder(View view) {
            super(view);
            taxName = view.findViewById(R.id.taxName);
            taxPercentage = view.findViewById(R.id.taxPercentage);
            bookingGst = view.findViewById(R.id.bookingGst);
        }
    }
}